import numpy as np
import pandas as pd
import lightgbm
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn as sns
import matplotlib.pyplot as plt   #Data visualisation libraries

# from sklearn.preprocessing import LabelBinarizer


#
# Prepare the data
#

train = pd.read_csv('sberbank.csv')
# train['price_doc'] = np.log1p(train['price_doc'])

ymean = train['price_doc'].mean()
print(ymean)
sns.distplot(train['price_doc'])
plt.show()

# train['timestamp'] = pd.to_datetime(train['timestamp'], format="%Y-%m-%d")
# # train['ts'] = pd.to_timedelta( pd.to_datetime(train['timestamp'], format="%Y-%m-%d"), unit='ns').dt.total_seconds().astype(int)
# #
# month_year = (train.timestamp.dt.month + (train.timestamp.dt.year - 2011) * 12)
# month_year_cnt_map = month_year.value_counts().to_dict()
# train['month_year_cnt'] = month_year.map(month_year_cnt_map)
#
# # Add week-year count
# week_year = (train.timestamp.dt.weekofyear + train.timestamp.dt.year * 100)
# week_year_cnt_map = week_year.value_counts().to_dict()
# train['week_year_cnt'] = week_year.map(week_year_cnt_map)
#
# # Add month and day-of-week
# train['month'] = train.timestamp.dt.month
# train['dow'] = train.timestamp.dt.dayofweek

# train['rel_floor'] = train['floor'] / train['max_floor'].astype(float)
# train['rel_kitch_sq'] = train['kitch_sq'] / train['full_sq'].astype(float)

train.drop(['timestamp'], axis=1, inplace=True)

# get the labels
y = train['price_doc']
train.drop(['id'], inplace=True, axis=1)

cats = train.select_dtypes(include='object').columns
le = preprocessing.LabelEncoder()
for col in cats:
    le.fit(train[col])
    train[col] = le.transform(train[col])

train.drop(['price_doc'], inplace=True, axis=1)
x = train.values

#
# Create training and validation sets
#
x, x_test, y, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

#
# Create the LightGBM data containers
#

categorical_features = [c for c, col in enumerate(train.columns) if col in cats]
train_data = lightgbm.Dataset(x, label=y, categorical_feature=categorical_features)
test_data = lightgbm.Dataset(x_test, label=y_test, categorical_feature=categorical_features)


#
# Train the model
#

parameters = {
    'objective': 'regression',
    # 'metric': 'mape',
    'metric': 'root_mean_squared_error',
    'is_unbalance': 'true',
    'boosting': 'gbdt',
    'num_leaves': 31,
    'feature_fraction': 0.5,
    'bagging_fraction': 0.5,
    'bagging_freq': 20,
    'learning_rate': 0.05,
    'verbose': 1
}

model = lightgbm.train(parameters,
                       train_data,
                       valid_sets=test_data,
                       num_boost_round=5000,
                       early_stopping_rounds=100)

rmse = model.best_score.get('valid_0')['rmse']
print(ymean, rmse)
relative = rmse /ymean
print(relative)

# expmean = np.exp(ymean)
# abs = np.exp(ymean + rmse) - expmean
# print(expmean, abs)
# relative = (abs)/np.exp(ymean)
# print(relative)


